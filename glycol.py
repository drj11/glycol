#!/usr/bin/env python

# glycol
# Glyph Count List

# for each font that this script can find (generally, system
# installed fonts), list the glyph count and the name

# This is designed to run on macOS using PyObjC
# https://pypi.org/project/pyobjc/
import Cocoa


def main():
    fonman = Cocoa.NSFontManager.sharedFontManager()
    names = [str(u) for u in fonman.availableFonts()]

    for name in names:
        # System fonts, which have a separate API, apparently.
        if name.startswith("."):
            continue
        font = Cocoa.NSFont.fontWithName_size_(name, 36)
        print(font.numberOfGlyphs(), font.fontName())


if __name__ == "__main__":
    main()
